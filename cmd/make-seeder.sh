#!/bin/bash

# --------------------------------------------------------------
# Create new seeder script
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------

# Get entire path of the script directory
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR
SCRIPT_DIR=$PWD

# Get sql directory path
SQL_DIR=$SCRIPT_DIR/../sql

# Get config (DATABASE_NAME var)
source ../config

# Defines seeder template path
SEEDER_TEMPLATE_PATH=$SQL_DIR/seeders/template/template.sql

# Init SEEDER_NAME to empty string
SEEDER_NAME=""
# Init TABLE_NAME to empty string
TABLE_NAME=""

# Defines the verbosity level
# Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH', default: 'HIGH'
VERBOSE_LEVEL=HIGH

# Init exit code, default: 0 (Success)
EXIT_CODE=0

# Show header function
show_header() {
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo -e "\e[36mCreate new seeder script\e[0m"
    echo -e "\e[36m@author Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo
}

# Show help function
show_help() {
    echo -e "Usage: make-seeder.sh \e[36m<table>\e[0m \e[33m[OPTIONS]\e[0m"
    echo
    echo -e "\e[36mARGS:\e[0m"
    echo
    echo -e "  \e[36m<table>\e[0m             Name of the target table."
    echo
    echo -e "\e[33mOPTIONS:\e[0m"
    echo
    echo -e "  \e[33m-h\e[0m                 Show this help."
    echo -e "  \e[33m-v\e[0m                 Defines the verbose level. Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH' (Default: 'HIGH')."
    echo
}

# Quit function
quit() {
    exit $EXIT_CODE
}

# --------------------------------------------------------------
# END INIT
# --------------------------------------------------------------

# Loop on passed arguments
#
# Solution found on stackoverflow to not give matter of OPTIONS/ARG position
# It can be :
#   - make-seeder.sh OPTIONS ARG
#   - make-seeder.sh ARG OPTIONS
#   - make-seeder.sh OPTION ARG OPTION
#   - etc...
# https://stackoverflow.com/questions/11742996/is-mixing-getopts-with-positional-parameters-possible
while [ $# -gt 0 ]
do
    unset OPTIND
    unset OPTARG
    # Get options
    # -h : To show command help
    # -v : To define verbose level
    while getopts "ht:v:" opt
    do
        case $opt in
            v) 
                case $OPTARG in
                    "NONE") VERBOSE_LEVEL=${OPTARG} ;;
                    "LOW") VERBOSE_LEVEL=${OPTARG} ;;
                    "MEDIUM") VERBOSE_LEVEL=${OPTARG} ;;
                    "HIGH") VERBOSE_LEVEL=${OPTARG} ;;
                    *) 
                        show_header
                        echo -e "\e[31mInvalid value for -v option\e[0m"
                        echo
                        show_help
                        EXIT_CODE=1 # Exit code: 1 (Error)
                        quit
                        ;;
                esac
                ;;
            h) 
                show_header
                show_help
                quit
                ;;
            *)
                show_header
                show_help
                EXIT_CODE=1 # Exit code: 1 (Error)
                quit
                ;;
        esac
    done
    shift $((OPTIND-1))
    ARGS="${ARGS:+${ARGS} }${1}"
    shift
done

# Build array of args
ARGS=($(echo $ARGS))
# Get first arg -> seeder name
TABLE_NAME=${ARGS[0]}

# --------------------------------------------------------------
# END SCRIPT OPTIONS PARSING
# --------------------------------------------------------------

# TABLE_NAME cannot be empty
if [ -z ${TABLE_NAME} ]; then
    show_header
    echo -e "\e[31mYou need to specify a table name\e[0m"
    echo
    show_help
    EXIT_CODE=1 # Exit code: 1 (Error)
    quit
fi

# Cleans TABLE_NAME (trim spaces)
TABLE_NAME=$(echo $TABLE_NAME)

# --------------------------------------------------------------
# END SCRIPT ARGS PARSING
# --------------------------------------------------------------

# Call show_header
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    show_header
fi

# --------------------------------------------------------------
# MAIN FUNCTION
# --------------------------------------------------------------

main() {

    #
    # 1 - Check if seeder file already exists
    #

    # Build file name
    SEEDER_FILE_NAME=$TABLE_NAME.sql
    # Defines full path
    SEEDER_FILE_PATH=$SQL_DIR/seeders/$SEEDER_FILE_NAME
    if [ -f "$SEEDER_FILE_PATH" ]; then
        echo -e "\e[31mERROR: seeder file 'seeders/$SEEDER_FILE_NAME' already exists.\e[0m"
        echo
        EXIT_CODE=1
        quit
    fi

    #
    # 2 - Get table columns
    #
    SHOW_COLUMNS_RESULT=$(mysql -D $DATABASE_NAME -sNe "SHOW COLUMNS FROM \`$TABLE_NAME\`;")

    # If last command exit code is error -> abort
    if [ $? -ne 0 ]; then
        EXIT_CODE=1
        quit
    fi

    TABLE_FIELDS="" # Empty string by default

    while read line ; do
        # Get first word -> field name
        field=$(echo $line | cut -d " " -f1)
        # Concatenate into TABLE_FIELDS
        TABLE_FIELDS="$TABLE_FIELDS\`$field\`,\n    "
    done <<< $(echo "$SHOW_COLUMNS_RESULT")

    # Remove the 7 last characters from $TABLE_FIELDS: ",\n    "
    TABLE_FIELDS=${TABLE_FIELDS::-7}

    #
    # 3 - Create seeder file based on the template
    #

    # Copy template to new file in seeders folder
    cp $SEEDER_TEMPLATE_PATH $SEEDER_FILE_PATH

    # If last command exit code is error -> abort
    if [ $? -ne 0 ]; then
        EXIT_CODE=1
        quit
    fi

    #
    # 4 - Replace values in the new file
    #

    # Remove the the five first line of the new file (Template comment)
    sed -i '1,5d' $SEEDER_FILE_PATH
    # Replace __TABLE_NAME__
    sed -i 's/__TABLE_NAME__/'"$TABLE_NAME"'/' $SEEDER_FILE_PATH
    # Replace __TABLE_FIELDS__
    sed -i 's/__TABLE_FIELDS__/'"$TABLE_FIELDS"'/' $SEEDER_FILE_PATH

    #
    # End - Success message
    #

    if [ $VERBOSE_LEVEL != "NONE" ]; then
        echo -e "\e[32mNew seeder '$TABLE_NAME' successfully created.\e[0m"
        echo -e "- $(realpath $SEEDER_FILE_PATH)"
        echo
    fi

}

# --------------------------------------------------------------

# Call main()
main

# Exit with success
quit