#!/bin/bash

# --------------------------------------------------------------
# Database CLI script
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------

# Get entire path of the script directory
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR
SCRIPT_DIR=$PWD

# Init COMMAND to empty string
COMMAND=""
# Init COMMAND_ARGS to empty string
COMMAND_ARGS=""

# Defines the verbosity level
# Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH', default: 'HIGH'
VERBOSE_LEVEL=HIGH

# Init exit code, default: 0 (Success)
EXIT_CODE=0

# Show header function
show_header() {
    echo -e "\e[36m---------------------------------------------------------------\e[0m"
    echo -e "\e[36m  ____        _        _                       ____ _     ___ \e[0m"
    echo -e "\e[36m |  _ \  __ _| |_ __ _| |__   __ _ ___  ___   / ___| |   |_ _|\e[0m"
    echo -e "\e[36m | | | |/ _\` | __/ _\` | '_ \ / _\` / __|/ _ \ | |   | |    | | \e[0m"
    echo -e "\e[36m | |_| | (_| | || (_| | |_) | (_| \__ \  __/ | |___| |___ | | \e[0m"
    echo -e "\e[36m |____/ \__,_|\__\__,_|_.__/ \__,_|___/\___|  \____|_____|___|\e[0m"
    echo
    echo -e "\e[36m @author Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo
    echo -e "\e[36m---------------------------------------------------------------\e[0m"
    echo
}

# Show help function
show_help() {
    echo -e "Usage: cli.sh \e[33m[OPTIONS]\e[0m \e[36mCOMMAND\e[0m [...<args>]"
    echo
    echo -e "\e[33mOPTIONS:\e[0m"
    echo
    echo -e "  \e[33m-h\e[0m                 Show this help."
    echo -e "  \e[33m-v\e[0m                 Defines the verbose level. Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH' (Default: 'HIGH')."
    echo
    echo -e "\e[36mCOMMAND:\e[0m"
    echo
    echo -e "(Specific command help can be displayed using -h option next to the command)"
    echo
    echo -e "  \e[36mcreate\e[0m             Create database"
    echo -e "  \e[36mmake:migration\e[0m     Create new migration file."
    echo -e "  \e[36mmake:seeder\e[0m        Create new seeder file."
    echo -e "  \e[36mmigrate\e[0m            Run one or all migrations."
    echo -e "  \e[36mmigrate:rollback\e[0m   Rollback one or all migrations."
    echo -e "  \e[36mmigrate:refresh\e[0m    Rollback and re-run all migrations."
    echo -e "  \e[36mreset\e[0m              Drop entire database and re-create it."
    echo -e "  \e[36mseed\e[0m               Run one or all seeders."
    echo
}

# Parse command
# Take full command as parameters
parse_command() {
    # Get the command
    COMMAND=$1
    # Get args (all other args)
    COMMAND_ARGS=${@:2}

    case $COMMAND in
        "create")
            to_exec () {
                # Create database
                $SCRIPT_DIR/database.sh -v MEDIUM $COMMAND_ARGS
            }
            ;;
        "make:migration")
            to_exec () {
                # Create migration
                $SCRIPT_DIR/make-migration.sh -v LOW $COMMAND_ARGS
            }
            ;;
        "make:seeder")
            to_exec () {
                # Create seeder
                $SCRIPT_DIR/make-seeder.sh -v LOW $COMMAND_ARGS
            }
            ;;
        "migrate")
            to_exec () {
                # Run migrations
                $SCRIPT_DIR/migrate.sh -v MEDIUM $COMMAND_ARGS
            }
            ;;
        "migrate:rollback")
            to_exec () {
                # Rollback migrations
                $SCRIPT_DIR/migrate.sh -v MEDIUM -R $COMMAND_ARGS
            }
            ;;
        "migrate:refresh")
            to_exec () {
                # First, rollback migrations
                $SCRIPT_DIR/migrate.sh -v MEDIUM -R
                # If exit code is error -> abort
                if [ $? -ne 0 ]; then
                    EXIT_CODE=1
                    quit
                fi
                # Then, re-run migrations
                $SCRIPT_DIR/migrate.sh -v MEDIUM
            }
            ;;
        "reset")
            to_exec () {
                $SCRIPT_DIR/database.sh -v MEDIUM -f $COMMAND_ARGS
            }
            ;;
        "seed")
            to_exec () {
                $SCRIPT_DIR/seed.sh -v MEDIUM $COMMAND_ARGS
            }
            ;;
        *)
            show_header
            echo -e "\e[31mUnknown command\e[0m"
            echo
            show_help
            EXIT_CODE=1 # Exit code: 1 (Error)
            quit
            ;;
    esac
}

# Quit function
quit() {
    exit $EXIT_CODE
}

# --------------------------------------------------------------
# END INIT
# --------------------------------------------------------------

# Get options (MUST BE THE FIRST ARGUMENTS)
# -h : To show command help
# -v : To define verbose level
while getopts "hv:" opt
do
    case $opt in
        v) 
            case $OPTARG in
                "NONE") VERBOSE_LEVEL=${OPTARG} ;;
                "LOW") VERBOSE_LEVEL=${OPTARG} ;;
                "MEDIUM") VERBOSE_LEVEL=${OPTARG} ;;
                "HIGH") VERBOSE_LEVEL=${OPTARG} ;;
                *) 
                    show_header
                    echo -e "\e[31mInvalid value for -v option\e[0m"
                    echo
                    show_help
                    EXIT_CODE=1 # Exit code: 1 (Error)
                    quit
                    ;;
            esac
            ;;
        h) 
            show_header
            show_help
            quit
            ;;
        *)
            show_header
            show_help
            EXIT_CODE=1 # Exit code: 1 (Error)
            quit
            ;;
    esac
done

shift $((OPTIND-1)) # remove the processed options

# --------------------------------------------------------------
# END SCRIPT OPTIONS PARSING
# --------------------------------------------------------------

# Parse command
parse_command $@

# --------------------------------------------------------------
# END SCRIPT ARGS PARSING
# --------------------------------------------------------------

# Call show_header
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    show_header
fi

# Display command name
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    echo -e "COMMAND: \e[33m$COMMAND\e[0m"
    echo
fi

# --------------------------------------------------------------
# MAIN FUNCTION
# --------------------------------------------------------------

main() {

    #
    # 1 - Do stuff based on command passed
    #

    # Exec built command
    to_exec
    EXIT_CODE=$?

}

# --------------------------------------------------------------

# Call main()
main

# Exit with success
quit