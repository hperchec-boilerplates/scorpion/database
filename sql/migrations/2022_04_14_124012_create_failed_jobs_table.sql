--
-- Migration: create_failed_jobs_table
--

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- Create failed_jobs table
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

  -- Create `failed_jobs` table
  CREATE TABLE `failed_jobs` (

    -- Fields
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `uuid` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
    `connection` text COLLATE utf8_unicode_ci NOT NULL,
    `queue` text COLLATE utf8_unicode_ci NOT NULL,
    `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
    `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
    `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

    -- Keys
    PRIMARY KEY (`id`),
    UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

  -- Drop failed_jobs table
  DROP TABLE `failed_jobs`;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;