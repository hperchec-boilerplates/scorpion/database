--
-- Migration: create_errors_table
--

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- Create errors table
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

  -- Create `errors` table
  CREATE TABLE `errors` (

    -- Fields
    `error_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Error unique id',
    `fr_message` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'FR translated message',
    `en_message` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'EN translated message',

    -- Keys
    PRIMARY KEY (`error_id`)

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

  -- Drop errors table
  DROP TABLE `errors`;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;